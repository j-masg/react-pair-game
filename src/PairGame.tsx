import React, { SetStateAction, useState } from 'react'
import './PairGame.css'

type State = 'default' | 'selected' | 'wrong' | 'success'
type Option = Record<"state" | "value" | "match", string>
type Selected = Option | undefined

function getState(option: Option, selected: Selected): State {
  if (!selected) {
    return 'selected'
  }

  if (option.value === selected?.match || selected.value === option.match) {
    return 'success'
  }

  return 'wrong'
}

function setState(o: Option, option: Option, selected: Selected, state: State): Option {
  if (o.value === selected?.value) {
    return { ...selected, state: state }
  }

  if (o.value === option.value) {
    return { ...option, state: state}
  }

  if (o.state !== 'success') {
    return { ...o, state: 'default'}
  }

  return o
}

function PairGame(props: {"data": Record<string, string>}): React.ReactNode {

  const primaries: string[] = Object.keys(props.data)
  const secondaries: string[] = Object.values(props.data)
  const optionsList: string[] = [...primaries, ...secondaries].sort(() => Math.random() - 0.5)
  const [options, setOptions]: [Option[], React.Dispatch<SetStateAction<Option[]>>] = useState(
    optionsList
      .map(option => ({
        state: 'default',
        value: option,
        match: props.data[option] ? props.data[option] : primaries.find(key => props.data[key] === option) || 'none'
      }))
      .filter(e => e.match !== 'none')
  );
  const [selected, setSelected]: [Selected, React.Dispatch<SetStateAction<Selected>>] = useState();

  function handleClick(option: Option): void {
    const state = getState(option, selected)

    setOptions(options.map(o => setState(o, option, selected, state)))

    if (state !== 'default' && state !== 'selected') {
      setSelected(undefined);
    } else {
      setSelected(option)
    }
  }

  function onReset(): void {
    const optionsList = options.map(o => ({...o, state: 'default'})).sort(() => Math.random() - 0.5)
    setOptions(optionsList)
  }

  return (
    <div className="button-container">
      {
        options.filter(option => option.state !== 'success').length === 0 
          ? <>Félicitations ! <button onClick={() => onReset()}>Rejouer ?</button></>
          : options.filter(option => option.state !== 'success').map(option => (
            <button className={option.state} key={option.value} onClick={() => handleClick(option)}>{option.value}</button>
          ))
      }
    </div>
  )
}

export default PairGame