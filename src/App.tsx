import './App.css'
import PairGame from './PairGame'

function App() {
  return (
    <>
      <div className="page-container">
        <h1>Welcome to this amazing pair game !</h1>
        <p>Simply match cards with their relevant pair. Don't complain if you get it wrong ! I make the rules here.</p>
      
        <PairGame data={{ Germany: 'Berlin', Azerbaijan: 'Baku', France: 'Paris', Japan: 'Tokyo' }}></PairGame>
      </div>
    </>
  )
}

export default App
