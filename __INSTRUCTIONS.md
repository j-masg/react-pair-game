# Create a pair game with react
In the game, the player needs to match a country to it's capital (or any other set of values) by clicking on appropriate buttons

1. OK : Your component should receive a data property in the following format:
`<PairGame data={{Germany: 'Berlin', Azerbaijan: 'Baku'}} />`

2. OK : A button should be displayed for each country and each capital.

3. OK : The buttons should be displayed in a random order.

4. OK : Clicking a button should set it's background color to blue (#fc2b2b)

5. Clicking another button should:
  - OK : remove both buttons if a correct pair has been selected
  - OK : set the background color of both buttons to red (#FFEEEE) if a wrong pair has been selected

6. OK : Assuming the previously selected pair was wrong, clicking another sbutton should restore the default background color of the wrong pair and set the background color of the clicked button to blue (as in point 4)

7. OK :When there are no buttons left, display a message: Congratulations.

8. OK :Export your component as the default export